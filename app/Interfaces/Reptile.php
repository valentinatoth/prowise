<?php

namespace App\Interfaces;

use App\Models\ReptileEgg;

interface Reptile
{
    public function layEgg(): ReptileEgg;
}
