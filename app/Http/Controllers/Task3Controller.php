<?php

namespace App\Http\Controllers;

use DateTime;

class Task3Controller extends Controller
{
    public function index(): void
    {
        $dates = $this->changeDateFormat(["2010/03/30", "15/12/2016", "11-15-2012", "20130720"]);

        foreach($dates as $date) {
            echo $date . "\n";
        }

        /*$dates = $this->changeDateFormatOtherSolution(["2010/03/30", "15/12/2016", "11-15-2012", "20130720"]);
        
        foreach($dates as $date) {
            echo $date . "\n";
        }*/
    }

    private function changeDateFormat(array $dates): array
    {
        $result = [];

        foreach ($dates as $date) {
            if (preg_match('/^\d{4}\/\d{2}\/\d{2}$/', $date)) {
                $result[] = DateTime::createFromFormat('Y/m/d', $date)->format('Ymd');
            } else if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $date)) {
                $result[] = DateTime::createFromFormat('d/m/Y', $date)->format('Ymd');
            } else if (preg_match('/^\d{2}\-\d{2}\-\d{4}$/', $date)) {
                $result[] = DateTime::createFromFormat('m-d-Y', $date)->format('Ymd');
            }
        }

        return $result;
    }

    private function changeDateFormatOtherSolution(array $dates): array
    {
        $result = [];

        foreach ($dates as $date) {
            if (preg_match('/^\d{4}\/\d{2}\/\d{2}$/', $date)) {
                $result[] = str_replace('/', '', $date);
            } else if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $date)) {
                $parts = explode('/', $date);
                $result[] = $parts[2].$parts[1].$parts[0];
            } else if (preg_match('/^\d{2}\-\d{2}\-\d{4}$/', $date)) {
                $parts = explode('-', $date);
                $result[] = $parts[2].$parts[0].$parts[1];
            }
        }

        return $result;
    }
}
