<?php

namespace App\Http\Controllers;

use App\Models\FireDragon;

class Task1Controller extends Controller
{
    public function index(): void
    {
        $dragon = new FireDragon();
        $egg = $dragon->layEgg();

        dump($egg);
        dump($egg->hatch());
        dump($egg);

        // Second hatch results Exception
        //dump($egg->hatch());
    }
}
