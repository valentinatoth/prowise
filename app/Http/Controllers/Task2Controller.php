<?php

namespace App\Http\Controllers;

class Task2Controller extends Controller
{
    public function index(): void
    {
        $arr = [1, 'a', 'b', 2];
        
        $this->filterNumbersFromArray($arr);

        print_r(array_values($arr));
    }

    private function filterNumbersFromArray(array &$arr): void
    {
        foreach ($arr as $key => $item) {
            if (gettype($item) !== 'integer') {
                unset($arr[$key]);
            }
        }
    }
}
