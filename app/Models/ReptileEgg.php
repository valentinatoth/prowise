<?php

namespace App\Models;

use App\Interfaces\Reptile;
use Exception;

class ReptileEgg
{
    private $reptileType;
    private $hatched = false;

    public function __construct(string $reptileType)
    {
        $this->reptileType = $reptileType;
    }

    public function hatch() : ?Reptile
    {
        if ($this->hatched) {
            throw new Exception();
        }

        $this->hatched = true;
        
        return new $this->reptileType();
    }
}
