<?php

namespace App\Models;

use App\Interfaces\Reptile;

class FireDragon implements Reptile
{
    public function layEgg(): ReptileEgg
    {
        return new ReptileEgg(FireDragon::class);
    }
}
